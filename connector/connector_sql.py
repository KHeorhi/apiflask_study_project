GET_CONNECTOR_LIST = """
        select loki.get_connector();
"""

GET_CONNECTOR_TABLE_LIST = """
        select * from loki.get_connector_table({connector_id});
"""

GET_TABLE_INFO = """
        select * from loki.get_table_info({connector_id}, {table_id});
"""
