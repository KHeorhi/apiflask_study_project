from apiflask import Schema
from apiflask.fields import Integer, List, String, Nested


class ConnectorListOut(Schema):
    connector_list = List(Integer)


class ConnectorTabelListOut(Schema):
    table_list = List(Integer)


class TableConditionsOut(Schema):
    field = String()
    date = String()


class ConnectorTabelInfoOut(Schema):
    schema_name = String()
    table_name = String()
    fields_list = List(String)
    conditions_list = Nested(TableConditionsOut)
