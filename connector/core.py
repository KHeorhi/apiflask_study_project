from db.db_interaction import db_query
from .connector_sql import GET_CONNECTOR_LIST, GET_CONNECTOR_TABLE_LIST, GET_TABLE_INFO


def get_connector():
    data_array = db_query(GET_CONNECTOR_LIST)
    return [data[0] for data in data_array]


def get_connector_table_list(params):
    data_array = db_query(GET_CONNECTOR_TABLE_LIST, params)
    return [data[0] for data in data_array]


def get_connector_table_info(params):
    data_array = db_query(GET_TABLE_INFO, params)
    return data_array
