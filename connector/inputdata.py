from apiflask import Schema
from apiflask.fields import Integer


class ConnectorTabelListIn(Schema):
    connector_id = Integer(required=True)


class ConnectorTabelInfoIn(Schema):
    connector_id = Integer(required=True)
    table_id = Integer(required=True)
