from apiflask import APIBlueprint, abort
from .core import get_connector, get_connector_table_list, get_connector_table_info
from .outputdata import ConnectorListOut, ConnectorTabelListOut, ConnectorTabelInfoOut
from .inputdata import ConnectorTabelInfoIn, ConnectorTabelListIn


connector = APIBlueprint('connector', __name__, url_prefix='/connector')


@connector.route('/get_connectors', methods=['GET'])
@connector.output(ConnectorListOut)
def get_connectors():
    data = get_connector()
    if isinstance(data, list):
        ConnectorListOut.connector_list = data
        return ConnectorListOut
    else:
        abort(404, 'No connector found')


@connector.route('/get_connector_table', methods=['GET'])
@connector.input(ConnectorTabelListIn, location='query')
@connector.output(ConnectorTabelListOut)
def get_connector_table(query_data):
    data = get_connector_table_list(params=query_data)
    ConnectorTabelListOut.table_list = data
    return ConnectorTabelListOut


@connector.route('/get_table_info', methods=['GET'])
@connector.input(ConnectorTabelInfoIn, location='query')
@connector.output(ConnectorTabelInfoOut)
def get_table_info(query_data):
    data = get_connector_table_info(params=query_data)
    for i in data:
        ConnectorTabelInfoOut.schema_name = i[0]
        ConnectorTabelInfoOut.table_name = i[1]
        ConnectorTabelInfoOut.fields_list = i[2]
        ConnectorTabelInfoOut.conditions_list = i[3]
    return {
        'schema_name': ConnectorTabelInfoOut.schema_name,
        'table_name': ConnectorTabelInfoOut.table_name,
        'fields_list': ConnectorTabelInfoOut.fields_list,
        'conditions_list': ConnectorTabelInfoOut.conditions_list
    }
