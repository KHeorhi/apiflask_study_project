\set schemaname loki

CREATE SCHEMA :schemaname;

CREATE TABLE IF NOT EXISTS :schemaname.connector(
  id             int primary key
 ,connector_name text not null
);

INSERT INTO :schemaname.connector (id, connector_name)
VALUES  (1, 'oracle_msfo'),
        (2, 'oracle_ipso'),
        (3, 'oracle_crm'),
        (4, 'postgres_bk');

CREATE TABLE IF NOT EXISTS :schemaname.table_info(
   id           int primary key
  ,table_schema text not null
  ,table_name   text not null
  ,fields       json not null
  ,condition    json
);

INSERT INTO loki.table_info (id, table_schema, table_name, fields, condition)
values  (2, 'loki', 'table_02', '["field_01", "field_02", "field_03", "field_04", "field_05", "field_06"]', '{"field":"field_02", "date": "2023-09-01 00:00:00" }'),
        (1, 'loki', 'table_01', '["field_01", "field_02", "field_03", "field_04", "field_05"]', '{"field":"field_02", "date": "2023-09-01 00:00:00" }'),
        (3, 'loki', 'table_03', '["field_01", "field_02", "field_03", "field_04", "field_05", "field_07"]', '{"field":"field_02", "date": "2023-09-01 00:00:00" }'),
        (4, 'loki', 'table_04', '["field_01", "field_02", "field_03", "field_04", "field_06", "field_08"]', '{"field":"field_02", "date": "2023-09-01 00:00:00" }'),
        (5, 'loki', 'table_05', '["field_01", "field_02", "field_03", "field_04", "field_06", "field_08", "field_09"]', '{"field":"field_02", "date": "2023-09-01 00:00:00" }');

CREATE TABLE IF NOT EXISTS :schemaname.db(
  id            int  primary key
 ,db_name       text not null
 ,db_credential json
);

INSERT INTO :schemaname.db (id, db_name, db_credential)
VALUES  (1, 'mpcs', null),
        (2, 'cl2',  null),
        (3, 'rtps', null),
        (4, 'hce',  null),
        (5, 'mcms', null),
        (6, 'acs',  null),
        (7, 'gp',   null);

CREATE TABLE IF NOT EXISTS :schemaname.connector_table_info(
   connector_id int references loki.connector (id)
  ,table_id     int references loki.table_info (id)
  ,source_id    int references loki.db (id)
  ,target_id    int references loki.db (id)
);

INSERT INTO :schemaname.connector_table_info (connector_id, table_id, source_id, target_id)
VALUES  (3, 3, 2, 7),
        (1, 5, 3, 7),
        (2, 1, 1, 7),
        (2, 4, 6, 7),
        (2, 2, 1, 7),
        (1, 1, 2, 7),
        (2, 4, 2, 7);

CREATE OR REPLACE FUNCTION :schemaname.get_connector()
returns table (
 connector_id int
)
language plpgsql as $$
begin
 return query
  select
   id
   from loki.connector;
end;
$$;

CREATE OR REPLACE FUNCTION :schemaname.get_connector_table(p_connector_id int)
returns table (
  connector_table_id int
) language plpgsql as $$
begin
  return query select
    distinct table_id
    from loki.connector_table_info
    where connector_id = p_connector_id;
end;
$$;

CREATE OR REPLACE FUNCTION :schemaname.get_table_info(p_connector_id int, p_table_id int)
returns table (
  f_schema_name text
 ,f_table_name  text
 ,f_fields      json
 ,f_condition   json
)
language plpgsql as $$
begin
 return query
  select
    table_schema
   ,table_name
   ,fields
   ,condition
   from loki.connector_table_info cf
    join loki.table_info ti on cf.table_id = ti.id
   where cf.connector_id = p_connector_id
    and cf.table_id = p_table_id;
end;
$$;
