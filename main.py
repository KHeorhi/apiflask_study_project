from apiflask import APIFlask
from connector.connector import connector


app = APIFlask(__name__, title='APIFlask_study', version='0.0.1', docs_path='/apidoc')
app.register_blueprint(connector)


if __name__ == '__main__':
    app.run(debug=True)
