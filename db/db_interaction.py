import psycopg2
from dotenv import dotenv_values
from typing import Any


config = dotenv_values("./.env")


def db_conn() -> Any:
    try:
        conn = psycopg2.connect(
         # host=config.get("PG_HOST"),
         # port=config.get("PG_PORT"),
         # database=config.get("PG_DB"),
         # user=config.get("PG_USER"),
         # password=config.get("PG_PWD")
            host='localhsot',
            port='5435',
            database='fjord',
            user='fjord',
            password='owner'
        )
        return conn
    except psycopg2.Error as error:
        return error.pgerror


def db_query(query: str, params: dict = None) -> Any:
    conn = db_conn()
    print(params)
    with conn.cursor() as cursor:
        with cursor as cursor_obj:
            if params is not None:
                query = query.format(**params)

            cursor_obj.execute(query)

            return cursor_obj.fetchall()
